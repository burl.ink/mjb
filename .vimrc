" Enable Pathogen
execute pathogen#infect()
syntax on
filetype plugin indent on

" Mistakes are made
inoremap <F1> <Esc>
map <F1> <Esc>
map q: :q
:command Q q
:command W w

" User Interface Improvements
let mapleader = "\<Space>"
set nu
set ruler
set whichwrap+=<,>,h,l
set hlsearch
set ignorecase
set smartcase
set incsearch
set showmatch
set backspace=2

" share clipboards with the system!
set clipboard=unnamed

highlight WhitespaceEOL ctermbg=red guibg=red
match WhitespaceEOL /\s\+$/

" User experience improvement
set lazyredraw
set magic
set shiftwidth=2
set tabstop=2
set softtabstop=2
set encoding=utf-8

"" Pep-8 Standards
" https://www.python.org/dev/peps/pep-0008/#tabs-or-spaces
" https://www.python.org/dev/peps/pep-0008/#maximum-line-length
autocmd BufEnter *.py.j2 setlocal filetype=python
autocmd FileType python setlocal timeoutlen=1000 ttimeoutlen=0 expandtab shiftwidth=4 tabstop=4 softtabstop=4 colorcolumn=80 smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd BufRead,BufNewFile,BufEnter */condor/*.py setlocal noexpandtab tabstop=4 softtabstop=4 shiftwidth=4


"" yaml alignment can be a real drag
autocmd FileType yaml setlocal invcursorcolumn
autocmd BufEnter *.yml.* setlocal filetype=yaml
autocmd BufEnter *.yaml.* setlocal filetype=yaml

"" robot framework
autocmd BufEnter *.robot setlocal invcursorcolumn expandtab shiftwidth=4 tabstop=4 softtabstop=4

"" Let's prefer tabs over spaces where possible
autocmd BufEnter *.sh.j2 setlocal filetype=sh
autocmd FileType sh setlocal softtabstop=4 tabstop=4 shiftwidth=4 noexpandtab

" Jump to end of pasted  pasted content
vnoremap <silent> y y`]
vnoremap <silent> p p`]
nnoremap <silent> p p`]

" From a talk by Damian Conway
" Improve incremental search results
function! HLNext (blinktime)
    set invcursorline
    redraw
    exec 'sleep ' . float2nr(a:blinktime * 1000) . 'm'
    set invcursorline
    redraw
endfunction
nnoremap <silent> n n:call HLNext(0.15)<cr>
nnoremap <silent> N N:call HLNext(0.15)<cr>

" Introduce a spelling function when writing documentation and the like
function! ToggleSpelling()
    if &spell
        echom "<( ^ _ ^ )> Spell it however you'd like!"
        set nospell
    else
        echom "<( o . o )> Spell it right!"
        set spell spelllang=en_us
    endif
endfunction
noremap <leader>s :call ToggleSpelling()<cr>

highlight Comment cterm=italic

let g:vimwiki_list = [{'path': '~/vimwiki', 'template_path': '~/vimwiki/templates/',
          \ 'template_default': 'default', 'syntax': 'markdown', 'ext': '.md*',
          \ 'path_html': '~/vimwiki/site_html/', 'custom_wiki2html': 'vimwiki_markdown',
          \ 'html_filename_parameterization': 1,
          \ 'template_ext': '.tpl'}]
